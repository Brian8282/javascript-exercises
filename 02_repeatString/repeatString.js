const repeatString = function(aString, aNum) {
    if (aNum < 0) {
        return "ERROR";
    }
    return aString.repeat(aNum);
};

// Do not edit below this line
module.exports = repeatString;
