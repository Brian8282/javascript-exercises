const removeFromArray = function(anArray, ...values) {
    let index;
    for (let x = 0; x < values.length; x++) {
        index = anArray.indexOf(values[x]);
        if (index > -1) {
            anArray.splice(index, 1);
        }
    }
    return anArray;
};

// Do not edit below this line
module.exports = removeFromArray;
