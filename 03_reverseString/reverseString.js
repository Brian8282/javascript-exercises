const reverseString = function(aString) {
    //Method 1: using charAt string method to find each character
    let newString = '';
    for (let i = aString.length - 1; i >= 0; i--) {
        newString += aString.charAt(i);
    }
    return newString;
    //Method 2: turning string to array to find each character
    /*
    let newString = aString.split("");
    let result = '';
    for (let i = newString.length - 1; i >= 0; i--) {
        result += newString[i];
    }
    return result;
    */
};

// Do not edit below this line
module.exports = reverseString;
