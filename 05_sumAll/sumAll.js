const sumAll = function(num1, num2) {
    let sum = 0, larger, smaller;
    if (typeof num1 != "number" || typeof num2 != "number") {
        return "ERROR";
    }
    if (num1 < 0 || num2 < 0) {
        return "ERROR";
    }
    if (num1 > num2) {
        larger = num1;
        smaller = num2;
    }
    else {
        larger = num2;
        smaller = num1;
    }
    for (let i = smaller; i <=larger; i++) {
        sum += i;
    }
    return sum;
};

// Do not edit below this line
module.exports = sumAll;
